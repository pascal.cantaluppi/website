import React from "react";

class Testimonials extends React.Component {
  render() {
    return (
      <div>
        <p>xxx</p>
        <section className="section pt-0">
          <div className="container">
            <div className="owl-carousel testimonial-carousel">
              <div className="testimonial-wrap">
                <div className="testimonial">
                  <img
                    src="assets/img/person_01.jpg"
                    alt="Jill"
                    className="img-fluid"
                  />
                  <blockquote>
                    <p>
                      Cloud-native Apps sind Innovationstreiber für
                      Transformationen und das digitale Business in pandemischen
                      Krisenzeiten.
                    </p>
                  </blockquote>
                  <p>&mdash; Jill Valentine</p>
                </div>
              </div>
              <div className="testimonial-wrap">
                <div className="testimonial">
                  <img
                    src="assets/img/person_02.jpg"
                    alt="Claire"
                    className="img-fluid"
                  />
                  <blockquote>
                    <p>
                      Dev-Ops Pipelines und automatisierte Git-Flows optimieren
                      das Testing und ermöglichen Deployments in skalierbare
                      Clusterumgebungen
                    </p>
                  </blockquote>
                  <p>&mdash; Claire Redfield</p>
                </div>
              </div>
              <div className="testimonial-wrap">
                <div className="testimonial">
                  <img
                    src="assets/img/person_03.jpg"
                    alt="Zombie"
                    className="img-fluid"
                  />
                  <blockquote>
                    <p>
                      Arrrg!
                      <br />
                      <br />
                      <br />
                    </p>
                  </blockquote>
                  <p>&mdash; Zombie</p>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}

export default Testimonials;
