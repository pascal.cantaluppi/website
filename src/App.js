import React from "react";
import Testimonials from "./components/Testimonials";

function App() {
  return (
    <React.Fragment>
      <Testimonials />
    </React.Fragment>
  );
}

export default App;
